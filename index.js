const express = require("express")

const mongoose = require("mongoose");

const app = express()
const port = 3001;

mongoose.connect(`mongodb+srv://alexisvnsy:admin123@zuittbatch197.8oalahx.mongodb.net/S35-Activity?retryWrites=true&w=majority`, {

	useNewUrlParser: true,

	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connection to MongoDB!'))

const usersSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	username: String,
	password: String,
	email: String
});

const productsSchema = new mongoose.Schema({
	name: String,
	description: String,
	price: String
});

const User = mongoose.model('User', usersSchema)
const Product = mongoose.model('Product', productsSchema)

app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.post('/register', (req, res) => {
	User.findOne({username: req.body.username}, (error, result) =>{
		if(error) {
			return res.send(error)
		} else if (result != null && result.username == req.body.username) {
			return res.send('Duplicate account found')
		} else {
			let newSign = new User({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				username: req.body.username,
				password: req.body.password,
				email: req.body.email

			})

			newSign.save((error, savedSign) => {
				if(error) {
					return console.error(error)
				} else {
					return res.status(201).send('New User Registered!')
				}
			})
		}
	})
});

app.post('/createproduct', (req, res) => {
	Product.findOne({name: req.body.name}, (error, result) =>{
		if(error) {
			return res.send(error)
		} else if (result != null && result.name == req.body.name) {
			return res.send('Duplicate product name found')
		} else {
			let newProd = new Product({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price

			})

			newProd.save((error, savedProduct) => {
				if(error) {
					return console.error(error)
				} else {
					return res.status(201).send('New Product Added to our inventory!')
				}
			})
		}
	})
});

app.get('/users', (req, res) => {
	User.find({}, (error, result) => {
		if(error) {
			return res.send(error)
		} else {
			return res.status(200).json({
				users: result
			})
		}
	})
});

app.get('/products', (req, res) => {
	Product.find({}, (error, result) => {
		if(error) {
			return res.send(error)
		} else {
			return res.status(200).json({
				products: result
			})
		}
	})
});




app.listen(port, () => console.log(`Server is running at port: ${port}`))